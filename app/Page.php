<?php

namespace App;

use Item;
use Slide;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{

	use SoftDeletes;


    protected $fillable = [
        'title',
        'body',
        'meta_description',
    ];


    public function slides()
    {

        return $this->hasMany('App\Slide');

    }

    public function items()
    {

    	return $this->hasMany('App\Item');

    }

    static function links(){

    	return Page::all()->take(10);
    }


}


