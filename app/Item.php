<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function page()
    {

    	return $this->belongsTo('App\Page');
    }
    
}
