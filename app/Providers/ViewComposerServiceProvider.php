<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Page;
use View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Page $page)
    {
        View::composer('partials.nav', function($view)
        {
            $view->with('links', Page::links());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
