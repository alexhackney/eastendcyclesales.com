<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

	public static function random(){

		$banner = Banner::inRandomOrder()->first();
		
		if ($banner != null) {
		
			$banner->increment('impressions');
		}

		return $banner;

	}
}
