<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', 'PagesController@home');
Route::get('/logout', 'PagesController@getLogout');
Route::get('contact', 'PagesController@contact');
Route::post('contact', 'PagesController@contactPost');

Route::group(['prefix' => 'admin', 'middleware' => 'auth.basic'], function(){
//	Route::get('/page/trash', 'PagesController@showTrash');
//	Route::get('/slide/trash', 'SlidesController@showTrash');

    Route::get('/', 'AdminController@home');
    Route::resource('page', 'PagesController');
    Route::resource('slide', 'SlidesController');
    Route::resource('items', 'ItemsController');
    Route::resource('banners', 'BannersController');
    Route::get('email', 'NewsletterController@index');

});

Route::post('/newsletter/signup', 'NewsletterController@add');


Route::get('{page_slug}', 'PagesController@showPageBySlug');
Route::get('{page_slug}/{item_slug}', 'PagesController@showItemBySlug');

