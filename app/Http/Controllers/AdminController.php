<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Banner;
use App\Item;
use App\Page;
use App\Slide;

class AdminController extends Controller
{
    public function home(){

    	$pages = Page::orderBy('updated_at', 'desc')->get()->take(3);

    	$items = Item::orderBy('created_at', 'desc')->get()->take(3);

    	$slides = Slide::orderBy('created_at', 'desc')->get()->take(3);

    	$banners = Banner::orderBy('created_at', 'desc')->get();

    	return view('admin.home', compact('pages', 'items', 'slides', 'banners'));

    }
}
