<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use Image;
use App\Page;

class ItemsController extends Controller
{
    public function index()
    {

    	$items = Item::all();

    	return view('admin.items.index', compact('items'));

    }

    public function create()
    {
    	$pages = Page::all();

    	return view('admin.items.create', compact('pages'));

    }

    public function store(Request $request)
    {

    	$item = new Item;

        $item->title = $request->title;
    	$item->slug = str_slug($request->title, '-');
    	$item->description = $request->description;
        $item->price = $request->price;
        $item->sale_price = $request->sale_price;

        if ($request->hasFile('item_photo')) {

            $file = $request->file('item_photo');

            $image = Image::make($file)->fit(1600, 900);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/items/uploaded/';

            $image->save($destinationPath . $fileName, 72);

            $item->image_path = $destinationPath . $fileName;

        }        

        $item->page_id = ($request->page_id);

        $item->save();

        return redirect('/admin/items');


    }


    public function edit($id)
    {

    	$item = Item::find($id);

    	$pages = Page::all();

    	return view('admin.items.edit', compact('item', 'pages'));

    }

    public function update(Request $request, $id)
    {

    	$item = Item::find($id);

    	$item->title = $request->title;
        $item->slug = str_slug($request->title, '-');
    	$item->description = $request->description;
        $item->price = $request->price;
        $item->sale_price = $request->sale_price;
        
        if ($request->hasFile('item_photo')) {

            $file = $request->file('item_photo');

            $image = Image::make($file)->fit(1600, 900);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/items/uploaded/';

            $image->save($destinationPath . $fileName, 72);

            $item->image_path = $destinationPath . $fileName;

        }        

        $item->page_id = ($request->page_id);

        $item->save();

        return redirect('/admin/items');
    }


    public function destroy($id)
    {

        $item = Item::find($id);

        $item->delete();

        return redirect('/admin/items');

    }
}
