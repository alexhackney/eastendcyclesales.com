<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Slide;
use Image;
use App\Page;

class SlidesController extends Controller
{
    public function index()
    {

    	$slides = Slide::all();

    	return view('admin.slide.index', compact('slides'));

    }

    public function create()
    {
    	$pages = Page::all();

    	return view('admin.slide.create', compact('pages'));

    }

    public function store(Request $request)
    {

    	$slide = new Slide;

    	$slide->title = $request->title;
    	$slide->meta_description = $request->meta_description;

        if ($request->hasFile('slide')) {

            $file = $request->file('slide');

            $image = Image::make($file)->fit(1920, 840);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/slides/uploaded/';

            $image->save($destinationPath . $fileName, 24);

            $slide->image_path = $destinationPath . $fileName;

        }        

        $slide->page_id = ($request->page_id);

        $slide->save();

        return redirect('/admin/slide');


    }


    public function edit($id)
    {

    	$slide = Slide::find($id);

    	$pages = Page::all();

    	return view('admin.slide.edit', compact('slide', 'pages'));

    }

    public function update(Request $request, $id)
    {

    	$slide = Slide::find($id);

    	$slide->title = $request->title;
    	$slide->meta_description = $request->meta_description;

        if ($request->hasFile('slide')) {

            $file = $request->file('slide');

            $image = Image::make($file)->fit(1920, 840);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/slides/uploaded/';

            $image->save($destinationPath . $fileName, 24);

            $slide->image_path = $destinationPath . $fileName;

        }        

        $slide->page_id = ($request->page_id);

        $slide->save();

        return redirect('/admin/slide');
    }


    public function destroy($id)
    {

        $slide = Slide::find($id);

        $slide->delete();

        return redirect('/admin');

    }
}
