<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Banner;
use Image;
use App\Page;

class BannersController extends Controller
{
    public function index()
    {

    	$banners = Banner::all();

    	return view('admin.banners.index', compact('banners'));

    }

    public function create()
    {
    	return view('admin.banners.create');

    }

    public function store(Request $request)
    {

    	$banner = new Banner;

        $banner->title = $request->title;
        $banner->slug = str_slug($request->title, '-');
    	$banner->alt_text = $request->alt_text;

        $url = preg_replace("(^https?://)", "", $request->url );
        
        $banner->url = $url;


        if ($request->hasFile('banner')) {

            $file = $request->file('banner');

            $image = Image::make($file)->fit(728, 90);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/banners/uploaded/';

            $image->save($destinationPath . $fileName, 72);

            $banner->image_path = $destinationPath . $fileName;

        }        

        $banner->save();

        return redirect('/admin/banners');


    }


    public function edit($id)
    {

    	$banner = Banner::find($id);

    	return view('admin.banners.edit', compact('banner'));

    }

    public function update(Request $request, $id)
    {

    	$banner = Banner::find($id);

    	$banner->title = $request->title;
        $banner->slug = str_slug($request->title, '-');
        $banner->alt_text = $request->alt_text;

        $url = preg_replace("(^https?://)", "", $request->url );
        
        $banner->url = $url;
        

        if ($request->hasFile('banner')) {

            $file = $request->file('banner');

            $image = Image::make($file)->fit(728, 90);

            $fileExtension = '.' . $file->getClientOriginalExtension();

            $fileName = str_slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName())) . $fileExtension;

            $destinationPath = 'img/banners/uploaded/';

            $image->save($destinationPath . $fileName, 72);

            $banner->image_path = $destinationPath . $fileName;

        }        

        $banner->save();

        return redirect('/admin/banners');
    }


    public function destroy($id)
    {

        $banner = banner::find($id);

        $banner->delete();

        return redirect('/admin');

    }

    public function getRandomBanner()
    {

        return Banner::all()->inRandomOrder()->first();         



    }

}
