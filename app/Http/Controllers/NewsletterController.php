<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Carbon\Carbon;

class NewsletterController extends Controller
{
    public function add(Request $request){

        $email = $request->email;

        if (DB::table('signups')->where('email', $email)->update(['updated_at' => Carbon::now()]))
            {
                    return back()->with('status', 'Thanks for subscribing to our newsletter!');
            }
        

        DB::table('signups')->insert([
            'email' => $request->email,
             'created_at' => Carbon::now()
            ]);

        
        return back()->with('status', 'Thanks for subscribing to our newsletter!');

    }

    public function index(){

        $signups = DB::table('signups')->get();

        return view('admin.signup.index', compact('signups'));

    }
}
