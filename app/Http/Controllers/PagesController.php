<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Page;
use App\Slide;
use App\Item;
use Auth;
use Mail;

class PagesController extends Controller
{

    public function index()
    {

        $pages = Page::all();
        
        return view('admin.page.index', compact('pages'));

    }
    public function create()
    {

        return view('admin.page.create');

    }

    public function store(Request $request)
    {
        $page = new Page;

        $page->title = $request->title;
        $page->menu_name = $request->menu_name;
        if($page->menu_name == 'Home'){
           $page->slug = '';
        }
        else {
            $page->slug = str_slug($request->menu_name);
        }
        $page->meta_description = $request->meta_description;
        $page->body = $request->body;

        $page->save();

        return redirect('/admin');


    }


    public function edit($id)
    {

        $page = Page::find($id);

        return view('admin.page.edit')->with('page', $page);

    }

    public function update(Request $request, $id)
    {

        $page = Page::find($id);

        $page->title = $request->title;
        $page->menu_name = $request->menu_name;
        if($page->menu_name == 'Home'){
           $page->slug = '';
        }
        else {
            $page->slug = str_slug($request->menu_name);
        }

        $page->meta_description = $request->meta_description;
        $page->body = $request->body;

        $page->save();

        return redirect('/admin');

    }

    public function showPageBySlug($page_slug)
    {
        $page = Page::where('slug', $page_slug)->firstOrFail();

        $slides = Slide::where('page_id', $page->id)->get();

        $items = Item::where('page_id', $page->id)->get();

        return view('page', compact('page', 'slides', 'items'));

    }

    public function home()
    {
        $page = Page::where('slug', '')->firstOrFail();

        $slides = Slide::where('page_id', $page->id)->get();

        return view('page')->with('page', $page)->with('slides', $slides);

    }

    public function contact()
    {

        return view('contact');

    }

    public function contactPost(Request $request)
    {

        Mail::send('emails.contact', ['request' => $request], function ($m) use ($request) {
                    $m->from(env('INFO_EMAIL'), env('INFO_NAME'));
                    $m->to(env('INFO_EMAIL'), env('INFO_NAME'));
                    $m->cc($request->email, $request->name);
                    $m->subject('Contact from ' . $request->name);
                });

        return redirect('/contact')->with('status', 'Your Email Was Recieved! Thanks For Contacting Us!');

    }


    public function destroy($id)
    {

        $page = Page::find($id);

        $page->delete();

        return redirect('/admin');
    }

    public function showTrashed()
    {

        //$pages = Page::all();

        dd('works');

        return view('page.index');

    }

    public function getLogout()
    {

        Auth::logout();

        return redirect('/');

    }

    public function showItemBySlug($page_slug, $item_slug)
    {
        $page = Page::where('slug', $page_slug)->firstOrFail();

        $item = Item::where('slug', $item_slug)->firstOrFail();
        
        $items = Item::where('page_id', $page->id)->inRandomOrder()->get()->except($item->id)->take(3);

        return view('item', compact('page', 'item', 'items'));
    }

}
