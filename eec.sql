-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: eastendcycle
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` text COLLATE utf8_unicode_ci NOT NULL,
  `alt_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `impressions` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `banners_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'2017 KX450/250','2017-kx450250','img/banners/uploaded/kawasaki20kx.jpg','East End cycle has the new Kawasaki KX450f','www.kawasaki.com/Products/2017-KX450F',12608,NULL,'2016-10-04 16:42:32','2017-02-06 13:57:53');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `page_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sale_price` double(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `items_title_unique` (`title`),
  UNIQUE KEY `items_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Kawasaki  2017 KX252','kawasaki-2017-kx252','img/items/uploaded/kx250.png','<h3>The Winning Edge</h3>\r\n<p>With more Supercross and Pro Motocross wins than all other OEMs combined, no one else comes close to the championship-winning power and race-ready technology of the Kawasaki KX<span class=\"superscript\" style=\"top: -1px; font-size: 8px;\"><sup>&trade;</sup></span>250F. The tradition continues in the all-new 2017 KX250F, The Bike That Builds Champions. Are you ready for the next level of championship-proven performance?</p>',7749.00,3,NULL,'2016-10-04 16:58:55','2016-10-04 17:02:35',0.00),(2,' Kawasaki  KX85 ','kawasaki-kx85','img/items/uploaded/2013-kawasaki-kx85a.jpg','<h2 style=\"text-align: center;\"><strong>NEW</strong></h2>\r\n<h2 style=\"text-align: center;\"><strong>2013&nbsp; KX85</strong></h2>\r\n<h2 style=\"text-align: center;\">&nbsp;</h2>\r\n<h2 style=\"text-align: center;\"><strong>***&nbsp;&nbsp; Website Special&nbsp;&nbsp; ****</strong></h2>\r\n<h2 style=\"text-align: center;\"><strong>&nbsp;</strong></h2>\r\n<h1 style=\"text-align: center;\"><strong> $3599.00</strong></h1>\r\n<p>&nbsp;</p>',4049.00,3,NULL,'2016-10-04 17:10:18','2016-10-22 16:19:58',3599.00),(3,'KAWASAKI  KSF 50','kawasaki-ksf-50','img/items/uploaded/d3n4h3i0-lsb.jpg','<h2><strong>THE KFX<span class=\"sup\">&reg;</span>50 ATV IS THE PERFECT FIRST ATV TO INTRODUCE NEW RIDERS SIX YEARS AND OLDER TO THE EXCITING FOUR-WHEEL LIFESTYLE.</strong></h2>',1999.00,4,NULL,'2016-10-05 16:25:04','2016-10-06 02:26:36',0.00),(4,'KAWASAKI KSF90','kawasaki-ksf90','img/items/uploaded/ksf90.jpg','<h2><strong>THE KFX<span class=\"sup\">&reg;</span>90 ATV PROVIDES THE IDEAL BLEND OF SIZE AND PERFORMANCE FOR RIDERS 12 AND OLDER THAT ARE STEPPING-UP FROM A 50<span style=\"text-transform: none;\">cc</span> ATV OR JUST GETTING STARTED.</strong></h2>',2599.00,4,NULL,'2016-10-05 16:37:33','2016-10-05 16:37:33',0.00),(5,'2013 HARLEY DAVIDSON ','2013-harley-davidson','img/items/uploaded/2013-harley-street-glide.jpg','<h2 style=\"text-align: center;\">LIKE NEW</h2>\r\n<h2 style=\"text-align: center;\">2013</h2>\r\n<h2 style=\"text-align: center;\">HARLEY DAVIDSON STREET GLIDE</h2>\r\n<h1 style=\"text-align: center;\">&nbsp;</h1>\r\n<h2 style=\"text-align: center;\"><strong>ONLY 237 MILES</strong></h2>',0.00,7,NULL,'2016-10-12 18:43:36','2016-10-25 16:51:42',0.00),(7,'Used Kawasaki VN800','used-kawasaki-vn800','img/items/uploaded/2003-vn800-2.jpg','<p style=\"text-align: center;\">Kawasaki Vulcan 800</p>\r\n<p style=\"text-align: center;\">Website Special</p>\r\n<h2 style=\"text-align: center;\">&nbsp;$2599.</h2>',2999.00,7,NULL,'2016-10-13 18:08:48','2016-10-14 17:51:55',2599.00),(9,'Kawasaki 1000 Ninja','kawasaki-1000-ninja','img/items/uploaded/2012-kawasaki-ninja1000c.jpg','<p style=\"text-align: center;\">NEW&nbsp; 2012</p>\r\n<p style=\"text-align: center;\">ZX1000</p>\r\n<p style=\"text-align: center;\">Website Special</p>\r\n<h2 style=\"text-align: center;\"><strong>$8999.00</strong></h2>\r\n<p>&nbsp;</p>',11199.00,3,NULL,'2016-10-22 16:16:51','2016-10-22 16:16:51',8999.00),(10,'ZX10R','zx10r','img/items/uploaded/10r.jpg','<p style=\"text-align: center;\">NEW</p>\r\n<p style=\"text-align: center;\">2013</p>\r\n<p style=\"text-align: center;\">KAWASAKI</p>\r\n<p style=\"text-align: center;\">ZX1000&nbsp; ZX-10R&nbsp; ABS</p>\r\n<p style=\"text-align: center;\">WEBSITE SPECIAL</p>\r\n<h2 style=\"text-align: center;\"><strong>$12,999.00</strong></h2>',15299.00,3,NULL,'2016-10-22 16:34:04','2016-10-22 18:42:58',12999.00);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_07_18_191725_create_newsletter_signups_table',1),('2016_08_12_041003_create_pages_table',1),('2016_08_12_041042_create_slides_table',1),('2016_10_04_014121_create_banners_table',2),('2016_10_03_225918_create_items_table',3),('2016_10_06_083334_add_sale_price_to_items_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'East End Cycles - Huntington, WV','Home','','<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 text-center\">\r\n<h2>Since 1972, East End Cycles is OUTDOORS. Off Road. On Road. Work. Play. On the Trail. Off the Trail.</h2>\r\n<p>Featuring Side-By-Sides, Motorcycles, ATV&rsquo;s, Outdoor Power Equipment, Pre-Owned and Accessories from Kawasaki, Artic Cat and Husqvarna. Over vehicles 100 in stock!</p>\r\n<p>&nbsp;</p>\r\n<h3>We have factory trained certified mechanics on staff in our service department.</h3>\r\n<p>&nbsp;</p>\r\n<p>East End Cycles is ready to fulfill your needs for OUTDOOR WORK, PLAY and FUN!</p>\r\n<h3>Hours: Tuesday-Friday 9 a.m. to 6 p.m.<br /> Saturday: 9 a.m. to 4 p.m.<br /> Closed Sunday &amp; Monday</h3>\r\n</div>\r\n</div>\r\n</div>','East End Cycles is ready to fulfill your needs for OUTDOOR WORK, PLAY and FUN! Featuring Side-By-Sides, Motorcycles, ATV’s, Outdoor Power Equipment, Pre-Owned and Accessories from Kawasaki, Artic Cat and Husqvarna.',NULL,'2016-08-12 09:00:24','2016-08-12 22:16:15'),(2,'We carry Side by Sides at East End Cycle in Huntington, WV','Side-By-Sides','side-by-sides','<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 text-center\">\r\n<h2>A Side By Side does it all &ndash; work and play, and East End Cycle has it all! East End Cycle is an authorized Kawasaki and Artic Cat Side By Side Dealer with more in stock than any other dealer in the area!</h2>\r\n<h4><strong>Right now get 0% APR on Pro Mules.</strong></h4>\r\n<hr /></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 text-center\">\r\n<h3>Visit our showroom today!</h3>\r\n</div>\r\n</div>\r\n</div>','See our inventory of Side by Sides at East End Cycle in Huntington, WV.',NULL,'2016-08-12 09:01:59','2016-08-12 22:39:39'),(3,'Kawasaki Motorcycles at East End Cycle in Huntington, WV','Motorcycles','motorcycles','<div class=\"container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h2>If four wheels is two too many, East End Cycle has the ride for you! East End Cycle has the full Kawasaki line of motorcycles including SuperSport, Touring, Cruisers, Off Road and Motorcross! Whether you’re on a back country road or hitting the trail – East End Cycle has the bike for you!</h2>\r\n			<hr>\r\n		</div>\r\n	</div>\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h3>Visit our showroom today!</h3>\r\n		</div>\r\n	</div>\r\n</div>','East End Cycle carries a large inventory of Kawasaki motorcycles.',NULL,'2016-08-12 09:02:24','2016-08-12 09:24:46'),(4,'Kawasaki and Arctic Cat ATVs at East End Cycle','ATVs','atvs','<div class=\"container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h2>All-terrain vehicles have to do it all: work, recreation, ranching, farming, hunting, and exploring.  Whether you’re hauling fence posts, up to your headlights in mud or staying dry on a smooth trail, East End Cycle has an ATV for you!</h2>\r\n			<hr>\r\n		</div>\r\n	</div>\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h3>Visit our showroom today!</h3>\r\n		</div>\r\n	</div>\r\n</div>','See our inventory of atvs from Kawasaki and Arctic Cat at East End Cycle.',NULL,'2016-08-12 09:02:54','2016-08-12 09:24:56'),(5,'Husqvarna Lawn and Garden tools at East End Cycle','Lawn and Garden','lawn-and-garden','<div class=\"container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h2>East End Cycles is a proud Husqvarna retailer offering a full range of forest and gardening outdoor power tools including lawn mowers, chainsaws, robotic mowers and more for both residential and commercial customers!</h2>\r\n			<hr>\r\n		</div>\r\n	</div>\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h3>Visit our showroom today!</h3>\r\n		</div>\r\n	</div>\r\n</div>','See our inventory of forest and gardening outdoor power tools including lawn mowers, chainsaws and robotic mowers at East End Cycle.',NULL,'2016-08-12 09:03:30','2016-08-12 09:25:04'),(6,'Power tools and Sports Accessories at East End Cycle','Accessories','accessories','<div class=\"container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h2>East End Cycles more than just your ride! They carry helmets, clothing, chest protectors, gloves, goggles, boots, saddlebags and more! Plus, East End Cycles has the parts to complete your vehicle and to keep it going, and can special order!</h2>\r\n			<hr>\r\n		</div>\r\n	</div>\r\n	<div class=\"row\">\r\n		<div class=\"col-md-12 text-center\">\r\n			<h3>Visit our showroom today!</h3>\r\n		</div>\r\n	</div>\r\n</div>','See our inventory accessories at East End Cycle.',NULL,'2016-08-12 09:04:03','2016-08-12 09:25:13'),(7,'Preowned motorcycles, atvs and power equipment at East End Cycle','PreOwned','preowned','<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 text-center\">\r\n<h2>Check back here often for the latest preowned offers from East End Cycles!</h2>\r\n<p><img src=\"http://3.bp.blogspot.com/-zjOWQf3RZLk/TpV4ujiy3KI/AAAAAAAAALg/PX69LSB30p8/s1600/2010-kawasaki-vulcan-1700-r-3-4.jpg\" alt=\"\" width=\"285\" height=\"182\" /></p>\r\n<p><strong>Available Now</strong></p>\r\n<p>2010&nbsp;Kawasaki Voyager 1700</p>\r\n<p>Call&nbsp;for pricing!</p>\r\n<p>&nbsp;</p>\r\n<hr /></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 text-center\">\r\n<h3>Visit our showroom today!</h3>\r\n</div>\r\n</div>\r\n</div>','See our preowned inventory at East End Cycle.',NULL,'2016-08-12 09:04:53','2016-08-12 22:42:21');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signups`
--

DROP TABLE IF EXISTS `signups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signups`
--

LOCK TABLES `signups` WRITE;
/*!40000 ALTER TABLE `signups` DISABLE KEYS */;
INSERT INTO `signups` VALUES (1,'mad12d.sb69@gmail.com','2016-08-12 08:59:22',NULL),(2,'cboyd662@gmail.com','2016-08-28 13:36:41',NULL),(3,'judsoncook3@gmail.com','2016-08-30 01:59:18',NULL),(4,'','2016-09-04 03:42:01','2016-12-19 01:16:50'),(5,'pyconley86@yahoo.com','2016-09-16 19:43:26',NULL),(6,'davydragon@aol.com','2016-11-12 16:53:53',NULL),(7,'Trishandjohn16@gmail.com','2016-12-08 11:46:25','2016-12-08 11:55:40'),(8,'kattycollector@tahoo.com','2016-12-10 19:18:45',NULL),(9,'kattycollector@yahoo.com','2016-12-10 19:19:04',NULL);
/*!40000 ALTER TABLE `signups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (1,'East End Cycle Sales','','img/slides/uploaded/east-end-cycle-sales.jpg','East End Cycle Sales',1,NULL,'2016-08-12 09:08:03','2016-08-12 09:08:03'),(2,'Side By Sides','','img/slides/uploaded/east-end-cycles-side-by-sides.jpg','East End Cycle Sells Side By Sides',1,NULL,'2016-08-12 09:08:30','2016-08-12 09:08:30'),(3,'Side By Sides','','img/slides/uploaded/side-by-sides.jpg','Side by Sides',1,NULL,'2016-08-12 09:09:02','2016-08-12 09:09:02'),(4,'Motorcycles','','img/slides/uploaded/motorcycles.jpg','New and Used Motorcycles',1,NULL,'2016-08-12 09:09:21','2016-08-12 09:09:21'),(5,'ATVs','','img/slides/uploaded/atvs.jpg','ATVs',1,NULL,'2016-08-12 09:09:32','2016-08-12 09:09:32'),(6,'Lawn and Garden Tools','','img/slides/uploaded/lawn-garden.jpg','Lawn and Garden Tools',1,NULL,'2016-08-12 09:09:48','2016-08-12 09:09:48'),(7,'We Sell Accessories','','img/slides/uploaded/accessories.jpg','Motorcycle, ATV and more, we have accessories for them all!',1,NULL,'2016-08-12 09:10:16','2016-08-12 09:10:16'),(8,'Artic Cat Prowler 700xt','','img/slides/uploaded/arctic-cat-prowler-700-xt.jpg','Artic Cat Prowler 700xt at East End Cycle',2,NULL,'2016-08-12 09:17:23','2016-08-12 09:17:23'),(9,'Artic Cat Wildcat x4','','img/slides/uploaded/arctic-cat-wildcat-4x.jpg','Artic Cat Wildcat x4 at East End Cycle',2,NULL,'2016-08-12 09:17:46','2016-08-12 09:17:46'),(10,'Kawasaki Side By Side','','img/slides/uploaded/kawasaki-side-by-side.jpg','Kawasaki Side By Side at East End Cycle Sales',2,NULL,'2016-08-12 09:18:13','2016-08-12 09:18:13'),(11,'Local Inventory of Side By Sides','','img/slides/uploaded/local-inventory-of-kawasaki-side-by-sides.jpg','Local Inventory of Side By Sides at East End Cycle Sales',2,NULL,'2016-08-12 09:18:38','2016-08-12 09:18:38'),(12,'Kawasaki Cruisers','','img/slides/uploaded/kawasaki-cruisers.jpg','Kawasaki Cruisers at East End Cycle Sales',3,NULL,'2016-08-12 09:19:07','2016-08-12 09:19:07'),(13,'Kawasaki Dirt Bikes','','img/slides/uploaded/kawasaki-dirt-bikes.jpg','Kawasaki Dirt Bikes at East End Cycle',3,NULL,'2016-08-12 09:19:27','2016-08-12 09:19:27'),(14,'Kawasaki Motorcycles','','img/slides/uploaded/kawasaki-motorcycles.jpg','Kawasaki Motorcycles at East End Cycle',3,NULL,'2016-08-12 09:19:47','2016-08-12 09:19:47'),(15,'Kawasaki Ninja Street Bikes','','img/slides/uploaded/kawasaki-ninja-street-bikes.jpg','Kawasaki Ninja Street Bikes at East End Cycle',3,NULL,'2016-08-12 09:20:11','2016-08-12 09:20:11'),(16,'Artic Cat trv700se','','img/slides/uploaded/arctic-cat-trv700se.jpg','Artic Cat trv700se',4,NULL,'2016-08-12 09:20:37','2016-08-12 09:20:37'),(17,'Artic Cat xc450','','img/slides/uploaded/arctic-cat-xc450.jpg','Artic Cat xc450',4,NULL,'2016-08-12 09:20:57','2016-08-12 09:20:57'),(18,'ATVs for All Ages','','img/slides/uploaded/kawasaki-atvs-for-all-ages.jpg','ATVs for All Ages',4,NULL,'2016-08-12 09:21:12','2016-08-12 09:21:12'),(19,'Kawasaki Brute Force ATVs','','img/slides/uploaded/kawasaki-brute-force-atvs.jpg','Get Kawasaki Brute Force ATVs at East End Cycle Sales',4,NULL,'2016-08-12 09:21:40','2016-08-12 09:21:40'),(20,'Husqvarna Chain Saws','','img/slides/uploaded/husqvarna-chain-saws.jpg','Husqvarna Chain Saws',5,NULL,'2016-08-12 09:22:04','2016-08-12 09:22:04'),(21,'Husqvarna Lawn Mowers','','img/slides/uploaded/husqvarna-lawn-mowers.jpg','Husqvarna Lawn Mowers',5,NULL,'2016-08-12 09:22:19','2016-08-12 09:22:19'),(22,'Husqvarna Leaf Blowers','','img/slides/uploaded/husqvarna-leaf-blowers.jpg','Husqvarna Leaf Blowers',5,NULL,'2016-08-12 09:22:34','2016-08-12 09:22:34'),(23,'Husqvarna Weed Eaters','','img/slides/uploaded/husqvarna-weed-eaters.jpg','Husqvarna Weed Eaters',5,NULL,'2016-08-12 09:22:49','2016-08-12 09:22:49'),(24,'Riding Gear','','img/slides/uploaded/all-types-of-riding-gear.jpg','Riding Gear',6,NULL,'2016-08-12 09:23:08','2016-08-12 09:23:08'),(25,'Helmets','','img/slides/uploaded/motorcycle-helmets.jpg','Helmets',6,NULL,'2016-08-12 09:23:22','2016-08-12 09:23:22'),(26,'Tires','','img/slides/uploaded/motorcycle-tires.jpg','Tires',6,NULL,'2016-08-12 09:23:36','2016-08-12 09:23:36'),(27,'Racing Goggles','','img/slides/uploaded/racing-goggles.jpg','Racing Goggles',6,NULL,'2016-08-12 09:23:54','2016-08-12 09:23:54'),(28,'Preowned Motorcycles and ATVs','','img/slides/uploaded/preowned-motorcyles.jpg','Preowned Motorcycles and ATVs',7,NULL,'2016-08-12 09:24:16','2016-08-12 09:24:16');
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Alex Hackney','me@alexhackney.com','$2y$10$BgwqqCcySss3GBn0/nhprejA2/aP8cqQnJrH4FdDEtvQnIz4tx37y','zgM3tGKQjaSOjy9DFaDikTGJPpUUJ8Okpl7tDFSapktNoGA7YF8mHcQW5okW','2016-08-12 08:59:10','2016-08-12 09:26:27'),(2,'East End','info@eastendcyclesales.com','$2y$10$ASvyUWRjE/nLa4tHIux3Su6DzUbGyavj7L3HtkXvy6sGmFEEAq9nG',NULL,'2016-08-12 21:24:07','2016-10-04 16:11:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-06  9:07:35
