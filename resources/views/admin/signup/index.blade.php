@extends('master')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h1>Email Signups!</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				@include('partials.tables.signups')
			</div>
		</div>
	</div>
	
@endsection