@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Create A Slide!</h1>
					</div>
				</div>

				<form action="/admin/slide" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}

				@include('partials.forms.slide')

				<button type="submit" class="btn btn-success btn-block">Create The Slide</button>

				</form>
			</div>
		</div>
	</div>
@endsection