@extends('master')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>All Slides</h1>
				@include('partials.tables.slides')
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<a href="/admin/slide/create" class="btn btn-success btn-block">Create a new Slide!</a>
			</div>
		</div>

	</div>
	
@endsection