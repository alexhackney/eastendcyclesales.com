@extends('master')

@section('content')
	<div class="container">
		<div class="row admin-home-section">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<h1>Admin Panel</h1>
							<h2>Latest Updated Site Pages</h2>
						</div>
					@include('partials.tables.pages')
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="/admin/page/create" class="btn btn-success btn-block">See All Pages</a>
					</div>
					<div class="col-md-6">
						<a href="/admin/page/create" class="btn btn-success btn-block">Create a New Page</a>
					</div>
				</div>
			</div>
		</div>

		<hr>
		<div class="row admin-home-section">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<h2>Latest Inventory Items</h2>
						</div>
					@include('partials.tables.items')
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="/admin/items" class="btn btn-success btn-block">See All Inventory Items</a>
					</div>
					<div class="col-md-6">
						<a href="/admin/items/create" class="btn btn-success btn-block">Create A Inventory Item</a>
					</div>
				</div>
			</div>
		</div>

		<hr>
		<div class="row admin-home-section">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<h2>Latest Slides</h2>
						</div>
					@include('partials.tables.slides')
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="/admin/slide" class="btn btn-success btn-block">See All Slides</a>
					</div>
					<div class="col-md-6">
						<a href="/admin/slide/create" class="btn btn-success btn-block">Create A New Slide</a>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row admin-home-section">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<h2>Active Banners</h2>
						</div>
					@include('partials.tables.banners')
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="/admin/banners" class="btn btn-success btn-block">See All Banners</a>
					</div>
					<div class="col-md-6">
						<a href="/admin/banners/create" class="btn btn-success btn-block">Create a New Banner</a>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection