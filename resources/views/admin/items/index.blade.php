@extends('master')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>All Items For Sale</h1>
				@include('partials.tables.items')
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<a href="/admin/items/create" class="btn btn-success btn-block">Create a new Item!</a>
			</div>
		</div>

	</div>
	
@endsection