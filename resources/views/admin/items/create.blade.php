@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Create an Inventory Item!</h1>
					</div>
				</div>

				<form action="/admin/items" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}

				@include('partials.forms.item')

				<button type="submit" class="btn btn-success btn-block">Add the item for sale!</button>

				</form>
			</div>
		</div>
	</div>
@endsection