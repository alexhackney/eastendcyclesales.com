@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Edit {{ $item->title }}</h1>
					</div>
				</div>

				<form action="/admin/items/{{ $item->id }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				{{ method_field('PATCH')}}

				@include('partials.forms.item')

				<button type="submit" class="btn btn-success btn-block">Update the Item</button>

				</form>
			</div>
		</div>
	</div>
@endsection