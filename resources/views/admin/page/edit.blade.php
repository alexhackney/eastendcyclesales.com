@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Edit </h1>
					</div>
				</div>

				<form action="/admin/page/{{ $page->id }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PATCH')}}

				@include('partials.forms.page')

				<button type="submit" class="btn btn-success btn-block">Update The Page</button>

				</form>
			</div>
		</div>
	</div>
@endsection