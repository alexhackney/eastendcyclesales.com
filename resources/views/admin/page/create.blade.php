@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Create A New Page!</h1>
						<p>Pages created show up in the main site automatically.</p>
					</div>
				</div>

				<form action="/admin/page" method="POST">
				{{ csrf_field() }}

				@include('partials.forms.page')

				<button type="submit" class="btn btn-success btn-block">Create The Page</button>

				</form>
			</div>
		</div>
	</div>
@endsection