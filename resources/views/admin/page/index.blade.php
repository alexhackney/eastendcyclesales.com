@extends('master')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Site Pages!</h1>
				@include('partials.tables.pages')
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<a href="/admin/page/create" class="btn btn-success btn-block">Create a new Page!</a>
			</div>
		</div>
	</div>
	
@endsection