@extends('master')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Banners</h1>
				@include('partials.tables.banners')
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<a href="/admin/banners/create" class="btn btn-success btn-block">Create a new Banner!</a>
			</div>
		</div>

	</div>
	
@endsection