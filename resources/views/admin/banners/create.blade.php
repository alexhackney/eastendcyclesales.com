@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Create A Banner!</h1>
					</div>
				</div>

				<form action="/admin/banners" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}

				@include('partials.forms.banner')

				<button type="submit" class="btn btn-success btn-block">Create The Banner</button>

				</form>
			</div>
		</div>
	</div>
@endsection