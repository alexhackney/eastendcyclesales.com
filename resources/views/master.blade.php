
<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		@yield('seo')
		<meta name="google-site-verification" content="6vOPIwGO0casDbKUbRQknZcqg9Lw5wejmhFveIGu3lw" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
		<script src="https://use.typekit.net/cib6pfo.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<link rel="stylesheet" href="/css/style.css">
		<!-- modernizr css -->
	</head>
	<body>
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

@include('partials.header')

@if (session('status'))
    <div class="alert alert-success text-center">
        {{ session('status') }}
    </div>
@endif
@yield('slider')
@yield('content')
<hr/>
@include('partials.footer')
		<script src="/js/app.js"></script>
@yield('footer-scripts')
@yield('analytics')
	</body>
</html>