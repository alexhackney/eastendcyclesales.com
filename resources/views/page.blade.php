@extends('master')

@section('seo')
<title>{{ $page->title }}</title>
<meta name="description" content="{{ $page->meta_description }}">
@endsection


@section('content')
@if(isset($slides))
	@include('partials.slider')
@endif

<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="page-title">{{ $page->title }}</h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			{!! $page->body !!}
		</div>
	</div>
</div>

<hr>
@if(isset($items) && count($items) > 0)
<div class="container">
	<div class="row">
		<h2 class="text-center">Items Currently For Sale</h2>
	</div>
</div>
	@include('partials.item-list')
@endif

@include('partials.newsletter-signup')
@endsection

@section('cta')
@include('partials.featured-products')
@endsection

@if(App::environment('production'))
	@include('partials.google-analytics')
@endif
