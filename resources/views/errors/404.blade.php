@extends('master')

@section('seo')
<title>404 Page Not Found!</title>
<meta name="description" content="Page Not Found">
@endsection


@section('content')
@if(isset($slides))
	@include('partials.slider')
@endif

<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="page-title">404 Page Not Found</h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<p>Looks like you got a little lost!</p>
			<p><a href="/">Try here!</a></p>
		</div>
	</div>
</div>

@endsection

@section('cta')
@include('partials.newsletter-signup')
@endsection