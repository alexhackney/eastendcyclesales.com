<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" placeholder="Inventory Item Title" value="{{ isset($item) ? $item->title : old('title') }}" required>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="price">Price</label>
			<input type="number" name="price" step="any" class="form-control" placeholder="14999.99" value="{{ isset($item) ? $item->price : old('price') }}">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="sale_price">Sale Price</label>
			<input type="number" name="sale_price" step="any" class="form-control" placeholder="14999.99" value="{{ isset($item) ? $item->sale_price : old('sale_price') }}">
		</div>
	</div>
</div>
<div class="form-group">
	<label for="meta_description">Description</label>
	<textarea name="description" class="form-control" placeholder="Item Details"required>
	{{ isset($item) ? $item->description : old('description') }}
	</textarea>
</div>
@if(isset($item))
<div class="form-group">
	<label for="current">Current Image</label>
	<img src="/{{ $item->image_path }}" class="img img-responsive">
</div>
@endif
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="item_photo">Image Upload</label>
			<input type="file" name="item_photo" id="item_photo">
			<p class="help-block">Upload a photo of the item. Large is better!</p>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="page_id">What category is this item?</label>
			<select class="form-control" name="page_id">
		  		@foreach ($pages as $page)
						@if (isset($item) && $page->id == $item->page->id)
		  					<option value="{{ $page->id }}" selected>{{ $page->menu_name }}</option> 
		  				@else
		  					<option value="{{ $page->id }}" >{{ $page->menu_name }}</option> 
		  				@endif
		  		@endforeach
			</select>
		</div>
	</div>
</div>


@section('footer-scripts')

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea',

 height: 300,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    '//www.tinymce.com/css/codepen.min.css'
  ]

   });</script>

@endsection