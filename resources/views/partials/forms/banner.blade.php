<div class="form-group">
	<label for="title">Title</label>
	<input type="text" name="title" class="form-control" placeholder="banner Title" value="{{ isset($banner) ? $banner->title : old('title') }}" required>
</div>
@if(isset($banner))
<div class="form-group">
	<label for="current">Current Image</label>
	<img src="/{{ $banner->image_path }}" class="img img-responsive">
</div>
@endif
<div class="form-group">
	<label for="alt_text">Alt Text (Describe the banner content)</label>
	<input type="text" name="alt_text" class="form-control" placeholder="What's in this image? One - Two Sentences" value="{{ isset($banner) ? $banner->alt_text : old('alt_text') }}" required>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label for="banner">Image Upload</label>
			<input type="file" name="banner" id="banner">
			<p class="help-block">Upload your image here, should be 1920x1080 or so.</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label for="url">Where should this banner link?</label>
			<input type="text" name="url" class="form-control" placeholder="http://link.com" value="{{ isset($banner) ? $banner->url : old('url') }}">
		</div>
	</div>
</div>
