<div class="form-group">
	<label for="title">Title</label>
	<input type="text" name="title" class="form-control" placeholder="Slide Title" value="{{ isset($slide) ? $slide->title : old('title') }}" required>
</div>
@if(isset($slide))
<div class="form-group">
	<label for="current">Current Image</label>
	<img src="/{{ $slide->image_path }}" class="img img-responsive">
</div>
@endif
<div class="form-group">
	<label for="meta_description">Description</label>
	<input type="text" name="meta_description" class="form-control" placeholder="What's in this image? One - Two Sentences" value="{{ isset($slide) ? $slide->meta_description : old('meta_description') }}" required>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="slide">Image Upload</label>
			<input type="file" name="slide" id="slide">
			<p class="help-block">Upload your image here, should be 1920x1080 or so.</p>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="page_id">What Page Is This For?</label>
			<select class="form-control" name="page_id">
		  		@foreach ($pages as $page)
						@if (isset($slide) && $page->id == $slide->page->id)
		  					<option value="{{ $page->id }}" selected>{{ $page->title }}</option> 
		  				@else
		  					<option value="{{ $page->id }}" >{{ $page->title }}</option> 
		  				@endif
		  		@endforeach
			</select>
		</div>
	</div>
</div>
