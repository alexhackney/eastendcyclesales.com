<div class="form-group">
	<label for="title">Title</label>
	<input type="text" name="title" class="form-control" placeholder="Page Title" value="{{ isset($page) ? $page->title : old('title') }}" required>
</div>
<div class="form-group">
	<label for="menu_name">Menu Item Name</label>
	<input type="text" name="menu_name" class="form-control" placeholder="Page Title" value="{{ isset($page) ? $page->menu_name : old('menu_name') }}">
</div>
<div class="form-group">
	<label for="meta_description">Description</label>
	<input type="text" name="meta_description" class="form-control" placeholder="What's the page about? One - Two Sentences" value="{{ isset($page) ? $page->meta_description : old('meta_description') }}" required>
</div>
<div class="form-group">
	<label for="body">Page Body</label>
	@if(isset($page))
	<textarea name="body" class="form-control">{!! $page->body !!}</textarea> 
	@else
	<textarea name="body" class="form-control"></textarea> 
	@endif
</div>

@section('footer-scripts')

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea',

 height: 500,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    '//www.tinymce.com/css/codepen.min.css'
  ]

   });</script>

@endsection