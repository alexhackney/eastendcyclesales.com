	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Page</th>
			<th>Title</th>
			<th>Image</th>
			<th>Last Updated At</th>
			<th>Actions</th>
			<th>Delete</th>
		</tr>
		@if(isset($slides))
		@foreach($slides as $slide)
		<tr>
			<td>{{ $slide->id }}</td>
			<td>{{ $slide->page->menu_name }}</td>
			<td>{{ $slide->title }}</td>
			<td><img src="/{{ $slide->image_path }}" class="img img-responsive" style="max-width:200px;"></td>
			<td>{{ $slide->updated_at }}</td>
			<td>
				<a href="/admin/slide/{{ $slide->id }}/edit" class="btn btn-warning">Edit</a>
				<a href="/{{ $slide->image_path }}" class="btn btn-success">View</a>
			</td>
			<td>
				<form action="/admin/slide/{{ $slide->id }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				<button href="/admin/slide/{{ $slide->id }}" class="btn btn-danger">Delete</button>
				</form>
				
			</td>
		</tr>
		@endforeach
		@endif


	</table>
