	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>URL</th>
			<th>Impressions</th>
			<th>Image</th>
			<th>Last Updated At</th>
			<th>Actions</th>
			<th>Delete</th>
		</tr>
		@if(isset($banners))
		@foreach($banners as $banner)
		<tr>
			<td>{{ $banner->id }}</td>
			<td>{{ $banner->title }}</td>
			<td><a href="{{ $banner->url }}" target="_blank">{{ $banner->url }}</a></td>
			<td>{{ $banner->impressions }}</td>
			<td><img src="/{{ $banner->image_path }}" class="img img-responsive" style="max-width:200px;"></td>
			<td>{{ $banner->updated_at }}</td>
			<td>
				<a href="/admin/banners/{{ $banner->id }}/edit" class="btn btn-warning">Edit</a>
				<a href="/{{ $banner->image_path }}" class="btn btn-success">View</a>
			</td>
			<td>
				<form action="/admin/banners/{{ $banner->id }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				<button href="/admin/banners/{{ $banner->id }}" class="btn btn-danger">Delete</button>
				</form>
				
			</td>
		</tr>
		@endforeach
		@endif


	</table>
