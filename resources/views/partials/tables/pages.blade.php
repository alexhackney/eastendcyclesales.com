	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>URL</th>
			<th>Created At</th>
			<th>Last Updated At</th>
			<th>Actions</th>
			<th>Delete</th>
		</tr>
		@if(isset($pages))
		@foreach($pages as $page)
		<tr>
			<td>{{ $page->id }}</td>
			<td>{{ $page->title }}</td>
			<td>{{ $page->slug }}</td>
			<td>{{ $page->created_at }}</td>
			<td>{{ $page->updated_at }}</td>
			<td>
				<a href="/admin/page/{{ $page->id }}/edit" class="btn btn-warning">Edit</a>
				<a href="/{{ $page->slug }}" class="btn btn-success">View</a>
			</td>
			<td>
				<form action="/admin/page/{{ $page->id }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				<button href="/admin/page/{{ $page->id }}" class="btn btn-danger">Delete</button>
				</form>
				
			</td>
		</tr>
		@endforeach
		@endif


	</table>
