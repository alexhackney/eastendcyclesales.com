	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Page</th>
			<th>Title</th>
			<th>Price</th>
			<th>Sale Price</th>
			<th>Image</th>
			<th>Last Updated At</th>
			<th>Actions</th>
			<th>Delete</th>
		</tr>
		@if(isset($items))
		@foreach($items as $item)
		<tr>
			<td>{{ $item->id }}</td>
			<td>{{ $item->page->menu_name }}</td>
			<td>{{ $item->title }}</td>
			@if($item->price > 0)
			<td>${{ $item->price }}</td>
			@else
			<td>Not Shown</td>
			@endif
			@if($item->sale_price > 0)
			<td>${{ $item->sale_price }}</td>
			@else
			<td>Not On Sale</td>
			@endif
			<td>
				<a href="/admin/items/{{ $item->id }}/edit">
					<img src="/{{ $item->image_path }}" class="img img-responsive" style="max-width:200px;">
				</a>
			</td>
			<td>{{ $item->updated_at }}</td>
			<td>
				<a href="/admin/items/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
				<a href="/{{ $item->page->slug }}/{{ $item->slug }}" class="btn btn-success">View</a>
			</td>
			<td>
				<form action="/admin/items/{{ $item->id }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				<button href="/admin/items/{{ $item->id }}" class="btn btn-danger">Delete</button>
				</form>
				
			</td>
		</tr>
		@endforeach
		@endif


	</table>
