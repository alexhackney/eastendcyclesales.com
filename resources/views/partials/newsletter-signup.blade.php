		<!--news-letter-area start -->
		<div class="news-letter-area news-letter-2 news-letter-4">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="news-letter">
							<div class="news-text">
								<h3><span>Subscribe</span> to our newsletter</h3>
								<p>Stay up to date on the latest product offerings and specials!</p>
							</div>
							<form action="/newsletter/signup" method="POST">
							{{ csrf_field() }}

								<div class="subscribe-box">
									<input type="email" name="email" placeholder="We protect your privacy!"required>
									<button type="submit">subscribe</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--news-letter-area end -->
