											@if(isset($links))
											@foreach($links as $link)
											<li><a href="/{{ $link->slug }}">{{ $link->menu_name }}</a></li>
											@endforeach
											@endif
											<li><a href="/contact">Contact</a></li>
											@if(Auth::check())

											<li><a href="/admin">Admin Panel</a>
												<ul>
													<li><a href="/admin/page">Pages</a></li>
													<li><a href="/admin/slide">Slides</a></li>
													<li><a href="/admin/items">Inventory</a></li>
													<li><a href="/admin/banners">Banners</a></li>
													<li><a href="/admin/email">Email Signups</a></li>
													<li><a href="/logout">Logout</a></li>
												</ul>
											</li>
											@endif