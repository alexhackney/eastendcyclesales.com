		<!-- Header-area start -->
		<header>
			<div class="header-area header-4-area">
				<!-- header-top start -->
				<div class="header-top">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
								<ul>
									<li><a href="https://www.google.com/maps/place/2402+3rd+Ave,+Huntington,+WV+25703/@38.4272456,-82.4183926,17z/data=!3m1!4b1!4m5!3m4!1s0x88460838c849edcd:0x6c1c8282d440ac09!8m2!3d38.4272414!4d-82.4162039" target="_blank">2402 3rd Ave Huntington, WV 25703</a></li>
									<li class="pull-right"><a href="callto:13045293309">(304)529-3309</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- header-top end -->
			@if(isset($banner))
				<div class="container banner">
					<div class="row">
						<div class="col-md-12">
							<div class="banner">
								<a href="http://{{ $banner->url }}"><img src="/{{ $banner->image_path }}" class="img img-responsive center-block" alt="{{ $banner->alt_text }}"></a>
							</div>
						</div>
					</div>
				</div>
			@endif

			<!-- logo start -->
				<div class="container">
					<div class="row">
						<div class="col-sm-4 hidden-xs">
							<img src="/img/brands/brands-left.png" class="logo-brands img img-responsive">
						</div>
						<div class="col-sm-4">
							<div class="logo text-center">
								<a href="/">
									<h3>East End Cycles</h3>
								</a>
								<a href="callto:13045233309">
									<h3>(304)529-3309</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-4">
							<img src="/img/brands/brands-right.png" class="logo-brands img img-responsive">
						</div>
					</div>
				</div>
			<!-- logo end -->

				<!-- header-bottom start -->
				<div class="header-bottom hidden-xs">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!-- main-menu start -->
								<div class="main-menu main-menu4">
									<nav>
										<ul>								
											@include('partials.nav')
										</ul>
									</nav>
								</div>
								<!-- main-menu end -->
							</div>
						</div>
					</div>
				</div>
				<!-- header-bottom end -->
			</div>
			<!-- mobile-menu-area start -->
			<div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
				<div class="container">
				<div class="row">
					<div class="col-md-12 hidden-lg hidden-md">
						<div class="mobile-menu">
							<nav id="dropdown">
								<ul>								
									@include('partials.nav')
								</ul>
							</nav>						
						</div>					
					</div>
				</div>
			</div>
			<!-- mobile-menu-area end -->
		</header>
		<!-- header area end -->
