@foreach($items->chunk(3) as $row)
<div class="container">
	<div class="row text-center item-row">
		<div class="col-md-12">
			@foreach($row as $item)
				@if(count($row) == 1)
				<div class="col-md-4 col-md-offset-4">
				@elseif(count($row) == 2)
				<div class="col-md-6">
				@else
				<div class="col-md-4">
				@endif
					<div class="row">
						<div class="col-md-12">
							<a href="/{{ $page->slug }}/{{ $item->slug }}">			
								<h2>{{ $item->title }}</h2>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="/{{ $page->slug }}/{{ $item->slug }}">
								<img src="/{{ $item->image_path }}" alt="$item->title">
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@if($item->price > 0)
							@if($item->sale_price > 0)
							<h3><span class="offsale-price">Price: ${{ $item->price }}</span><br><span class="sale-price"> Now Just: ${{ $item->sale_price }}</h3>
							@else
							<h3 class="">Price: ${{ $item->price }}</h3><br/>
							@endif
							@else
							<h4><a href="callto:13045293309">Call For Price!</a></h4>
							@endif

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="/{{ $page->slug }}/{{ $item->slug }}" class="btn btn-block btn-success">
								Learn More!
							</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endforeach