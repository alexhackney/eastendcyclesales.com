		<!-- slider-area start -->
		<div class="slider-area">
			<!-- slider start -->
			<div class="slider">
				<div id="topSlider" class="nivoSlider nevo-slider">
					@foreach($slides as $slide)
					<img src="{{ $slide }}" alt="See the {{ ucwords(implode(explode('-', str_replace('.jpg', '', explode('/', $slide)[3])),' ')) }} at East End Cycle"/>
					@endforeach
				</div>
			</div>
			<!-- slider end -->
		</div>
		<!-- slider-area end -->
