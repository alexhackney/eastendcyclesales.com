		<!-- slider-area start -->
		<div class="slider-area">
			<!-- slider start -->
			<div class="slider">
				<div id="topSlider" class="nivoSlider nevo-slider">
					@foreach($slides as $slide)
					<img src="/{{ $slide->image_path}}" alt="{{ $slide->title }}" />
					@endforeach
				</div>
			</div>
			<!-- slider end -->
		</div>
		<!-- slider-area end -->
