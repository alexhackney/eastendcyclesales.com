		<!-- banner-area start -->
		<div class="banner-area fix">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-banner">
							<a href="/blinds"><img src="img/products/blinds.jpg" alt=""></a>
						</div>
						<div class="single-banner">
							<a class="mar-banner" href="/flooring"><img src="img/products/flooring.jpg" alt=""></a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-banner">
							<a href="/fabrics"><img src="img/products/fabrics-370x550.jpg" alt=""></a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-banner">
							<a href="/windows"><img src="img/products/windows.jpg" alt=""></a>
						</div>
						<div class="single-banner">
							<a class="mar-banner" href="/reupholstery"><img src="img/products/reupholstery.jpg" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- banner-area end -->
