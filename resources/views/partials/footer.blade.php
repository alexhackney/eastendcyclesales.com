		<!--footer-area start -->
		<footer>
			<div class="footer-area footer-2-area">
				<div class="footer-middle-area ">
					<div class="container">
						<div class="footer-middle">
							<div class="row">
								<div class="col-md-4 col-sm-5 col-xs-12">
									<!-- middle-footer-text start-->
									<div class="middle-footer-text middle-footer-text4">
										<div class="footer-logo">
											<div class="logo text-center">
												<a href="/">
													<h3>East End Cycles</h3>
												</a>
												<a href="callto:13045293309">
													<h3>(304)529-3309</h3>
												</a>
											</div>
										</div>
										<div class="middle-text">
											<p>Since 1972, East End Cycles is OUTDOORS. Off Road. On Road. Work. Play. On the Trail. Off the Trail. Featuring Side-By-Sides, Motorcycles, ATV’s, Outdoor Power Equipment, Pre-Owned and Accessories from Kawasaki, Arctic Cat and Husqvarna. Over vehicles 100 in stock!</p>
											<ul class="footer-icon">
												
											</ul>
										</div>
									</div>
									<!-- middle-footer-text end-->
								</div>
								<div class="col-md-3 col-md-offset-1 col-sm-5 col-xs-12">
									<!-- middle-footer-text start-->
									<div class="middle-footer-text middle-footer-text2">
										<h3>contact us</h3>
										<div class="footer-address">
											<ul>
												<li><i class="fa fa-map-marker"></i>2402 3rd Ave<br/>Huntington, WV 25703</li>
												<li><i class="fa fa-phone"></i>(304)529-3309</li>
												<li><i class="fa fa-envelope-o"></i>info@eastendcyclesales.com</li>
											</ul>
										</div>
									</div>
									<!-- middle-footer-text end-->
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<!-- middle-footer-text start-->
									<div class="middle-footer-text middle-footer-text2">
										<h3>Our Hours</h3>
										<div class="footer-address">
											<ul>
												<li>Tuesday - Friday<br>9 a.m. to 6 p.m.</li>
												<li>Saturday<br>9 a.m. to 4 p.m.</li>
												<li>Closed Sunday &amp; Monday</li>
											</ul>
										</div>
									</div>
									<!-- middle-footer-text end-->
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<!-- middle-footer-text start-->
									<div class="middle-footer-text middle-footer-text2">
										<h3>Quick Menu</h3>
										<div class="footer-menu">
											<nav>
												<ul>								
													@include('partials.nav')
												</ul>
											</nav>
										</div>
									</div>
									<!-- middle-footer-text end-->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--footer-middle-area end -->
				<!--footer-bottom-area start -->
				<div class="footer-bottom-area footer-bottom2-area">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 text-center">
								<div class="footer-copyright">
									<p>Copyright © 2018 by <a href="http://eastendcyclesales.com">East End Cycles</a></p>
									<p>Website By <a href="http://kindredkonnect.com">Kindred Konnect</a> and <a href="https://alexhackney.com">Alex Hackney</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--footer-bottom-area end -->
			</div>
		</footer>
