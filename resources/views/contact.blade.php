@extends('master')
@section('seo')
<title>Contact East End Cycle Sales (304)529-3309</title>
<meta name="description" content="East End Cycle Sales is located at 2402 3rd Ave Huntington, WV 25703. Call us at (304)529-3309.">
@endsection


@section('content')

<div class="container">
		<div class="contact-page-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					<h1 class="text-center">Contact a sales professional at East End Cycles today!</h1>
						<div class="row">
							<div class="col-md-12">
								<!-- Map area -->
								<div class="map-area">
									<div id="googleMap" style="width:97%;height:410px;"></div>
								</div><!-- End Map area -->
							</div>
						</div>
						<div class="row">
							<!-- contact info -->
							<div class="col-md-6">
								<div class="contact-info">
									<h3>Contact Info</h3>
									<ul>
										<li>
											<i class="fa fa-map-marker"></i> <strong>Address</strong>
											2402 3rd Ave Huntington, WV 25703
										</li>
										<li>
											<i class="fa fa-envelope"></i> <strong>Phone</strong>
											(304)529-3309 Address:
										</li>
										<li>
											<i class="fa fa-mobile"></i> <strong>Email</strong>
											<a href="#">info@eastendcyclesales.com</a>
										</li>
									</ul>
								</div>
							</div><!-- End contact info -->
							<div class="col-md-5">
								<div class="contact-form">
									<h3><i class="fa fa-envelope-o"> </i> Send Us A Message</h3>
									<div class="row">
										<form action="/contact" method="post">
										{{ csrf_field() }}
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<input name="name" type="text" placeholder="Name (required)" />
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<input name="email" type="email" placeholder="Email (required)" />
											</div>
											<div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
												<input name="subject" type="text" placeholder="Subject" />
											</div>
											<div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
												<textarea name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
												<input type="submit" value="Submit Form" />
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div></div>

@endsection

@section('footer-scripts')
		<!-- Google Map js -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU"></script>
		<script>
			function initialize() {
			  var mapOptions = {
				zoom: 15,
				scrollwheel: false,
				center: new google.maps.LatLng(38.427242, -82.416191)
			  };

			  var map = new google.maps.Map(document.getElementById('googleMap'),
				  mapOptions);


			  var marker = new google.maps.Marker({
				position: map.getCenter(),
				//animation:google.maps.Animation.BOUNCE,
				icon: 'img/logo/map-marker.png',
				map: map
			  });

			}

			google.maps.event.addDomListener(window, 'load', initialize);
		</script>

@endsection
@if(App::environment('production'))
	@include('partials.google-analytics')
@endif
