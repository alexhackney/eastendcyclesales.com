@extends('master')

@section('seo')
<title>{{ $item->title }}</title>
<meta name="description" content="{{ $item->title }} available for sale from East End Cycle in Huntington, WV. Call (304)529-3309 for more information.">
@endsection


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="page-title">{{ $item->title }}</h1>
			@if($item->price > 0)
			@if($item->sale_price > 0)
			<h3 class="offsale-price">Regular Price: ${{ $item->price }}</h3>
			<h3 class="sale-price">Sale Price: ${{ $item->sale_price }}</h3>
			@else
			<h3 class="">Available Today For ${{ $item->price }}</h3>
			@endif
			@else
			<h3 class="">Call Us For Pricing Information!</h3>
			@endif

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<img src="/{{ $item->image_path }}" alt="{{ $item->title }}" class="img img-responsive">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			{!! $item->description !!}
		</div>
	</div>
</div>

<hr>
@if(isset($items))
<div class="container">
	<div class="row">
		<h2 class="text-center">Related Items</h2>
	</div>
</div>
	@include('partials.item-list')
@endif

@include('partials.newsletter-signup')
@endsection

@section('cta')
@include('partials.featured-products')
@endsection

@if(App::environment('production'))
	@include('partials.google-analytics')
@endif
