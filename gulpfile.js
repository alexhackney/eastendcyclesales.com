var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.styles([
		'bootstrap.min.css',
		'nivo-slider.css',
		'jquery-ui.min.css',
		'meanmenu.min.css',
		'owl.carousel.css',
		'font-awesome.min.css',
		'jquery.simpleGallery.css',
		'jquery.simpleLens.css',
		'style.css',
		'responsive.css',
        ],
        'public/css/style.css'
	);

	mix.scripts([
		'modernizr-2.8.3.min.js',
		'jquery-1.12.0.min.js',
		'bootstrap.min.js',
		'jquery.nivo.slider.pack.js',
		'owl.carousel.min.js',
		'jquery-ui.min.js',
		'jquery.meanmenu.js',
		'jquery.simpleGallery.min.js',
		'jquery.simpleLens.min.js',
		'wow.min.js',
		'plugins.js',
		'main.js',

        ],
        'public/js/app.js'
	);

});
